<?php
function asset_path($sub_path = '')
{
    $path = '';
    if (function_exists('get_template_directory_uri')) {
        $path .= '/vendor/devws/unified-header/images';
    } else {
        $path .= asset('unified-header-images');
    }
    if (!empty($sub_path) and strpos($sub_path, '/') !== 0) {
        // 서브 패스가 /로 시작하지 않으면 맨 앞에 /를 붙인다.
        $sub_path = '/' . $sub_path;
    }
    if (mb_strcut($sub_path, -1, 1) === '/') {
        // 서브 패스가 /로 끝나면 뗀다.
        $sub_path = mb_strcut($sub_path, 0, -1);
    }
    return $path.$sub_path;
}

function newspaper_url() {
    if (function_exists('get_template_directory_uri')) {
        return NEWSPAPER_URL ?? 'https://wspaper.org';
    } else {
        return env('NEWSPAPER_URL', 'https://wspaper.org');
    }
}

function journal_url() {
    if (function_exists('get_template_directory_uri')) {
        return JOURNAL_URL ?? 'https://marx21.or.kr';
    } else {
        return env('JOURNAL_URL', 'https://marx21.or.kr');
    }
}

function org_url() {
    if (function_exists('get_template_directory_uri')) {
        return ORG_URL ?? 'https://workerssolidarity.org';
    } else {
        return env('ORG_URL', 'https://workerssolidarity.org');
    }
}

function uh_is_active_menu($url = '') {
    if (!$url) {
        return false;
    }
    if (strpos(site_url(), $url) === 0) {
        return true;
    }
    return false;
}

function uh_search_form_action() {
    if (function_exists('get_template_directory_uri')) {
        return '/';
    } elseif (config('app.name') === '마르크스21') {
        return '/search';
    } else {
        return '/cse';
    }
}

function uh_search_input_name() {
    if (function_exists('get_template_directory_uri')) {
        return 's';
    } else {
        return 'keyword';
    }
}

function uh_need_header_search_form() {
    if (isset($_SERVER['HTTP_HOST']) and !strstr(journal_url(), $_SERVER['HTTP_HOST'])) {
        // 마르크스21이 아니면 무조건 노출
        return true;
    } elseif (empty($_GET[uh_search_input_name()])) {
        // 마르크스21이라도 검색 GET 변수가 세팅돼 있지 않으면 노출
        return true;
    }
    return false;
}
