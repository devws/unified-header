<div class="c-mobile-search  u-no-print">
    <label class="c-mobile-search__icon" for="mobile-search-toggle">
        <img width="20" height="20"
             src="<?php echo asset_path('/icon-magnifying-glass.svg') ?>" alt="검색창 열기">
    </label>

    <input class="c-mobile-search__toggle  js-mobile-search-toggle  u-hidden-visually" id="mobile-search-toggle"
           type="checkbox">

    <form action="<?= uh_search_form_action() ?>" method="get" class="c-mobile-search__form  u-bg-white  u-12/12">
        <label for="mobile-search-toggle" class="c-mobile-search__close">
            <img width="21" height="21" class="c-mobile-search__close-image"
                 src="<?php echo asset_path('/icon-chevron-up.svg') ?>" alt="검색창 닫기">
        </label>
        <input required type="search" value="<?= $_GET[uh_search_input_name()] ?? '' ?>"
               class="c-mobile-search__input  js-mobile-search-input" name="<?= uh_search_input_name() ?>"
               placeholder="검색어를 입력해 주세요">
        <button type="submit" class="c-mobile-search__submit">
            <img width="20" height="20"
                 src="<?php echo asset_path('/icon-magnifying-glass.svg') ?>" alt="검색하기">
        </button>
    </form>
    <div class="c-mobile-search__cover  js-mobile-search-cover">
    </div>
</div>
