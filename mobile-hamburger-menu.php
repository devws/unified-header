<?php
/**
 * @var array $main_nav
 */
?>
<div class="c-mobile-hamburger-menu  js-mobile-hamburger-menu">

    <div class="u-margin-bottom-large  u-text-center">
        <a href="/">
            <img class="lazyload" width="118" height="29" loading="lazy"
                 src="<?= asset_path().'/logo-unified-gray.svg' ?>"
                 alt="노동자연대">
        </a>
    </div>

    <div class="c-mobile-hamburger-menu__icon-wrapper  u-text-center  u-margin-bottom">
        <a class="u-inline-block  js-fb-page-link"
           href="https://play.google.com/store/apps/details?id=com.left21.alimi">
            <img width="24" height="24" class="lazyload" alt="안드로이드 앱"
                 loading="lazy" src="<?= asset_path().'/icon-playstore-gray.svg' ?>">
        </a>
        <a class="u-inline-block  js-fb-page-link"
           href="https://apps.apple.com/kr/app/%EB%85%B8%EB%8F%99%EC%9E%90-%EC%97%B0%EB%8C%80/id1559671489">
            <img width="24" height="24" class="lazyload" alt="아이폰 앱"
                 loading="lazy" src="<?= asset_path().'/icon-appstore-gray.svg' ?>">
        </a>
        <a class="u-inline-block  js-fb-page-link"
           href="<?= facebook_page_link() ?>">
            <img width="24" height="24" class="lazyload" alt="페이스북 페이지"
                 loading="lazy" src="<?= asset_path().'/icon-facebook-gray.svg' ?>">
        </a>
        <a class="u-inline-block"
           href="https://twitter.com/wspaper">
            <img width="24" height="24" class="lazyload" alt="트위터"
                 loading="lazy" src="<?= asset_path().'/icon-twitter-gray.svg' ?>">
        </a>
        <a class="u-inline-block"
           href=" https://www.instagram.com/wspaper_org/">
            <img width="24" height="24" class="lazyload" alt="인스타그램"
                 loading="lazy" src="<?= asset_path().'/icon-instagram-gray.svg' ?>">
        </a>
        <a class="u-inline-block"
           href="https://www.youtube.com/c/%EB%85%B8%EB%8F%99%EC%9E%90%EC%97%B0%EB%8C%80TV?sub_confirmation=1">
            <img width="24" height="24" class="lazyload" alt="유튜브 노동자연대TV"
                 loading="lazy" src="<?= asset_path().'/icon-youtube-gray.svg' ?>">
        </a>
        <a class="u-inline-block"
           href="https://wspaper.org/email/form">
            <img width="24" height="24" class="lazyload" alt="메일링"
                 loading="lazy" src="<?= asset_path().'/icon-mail-gray.svg' ?>">
        </a>
        <a class="u-inline-block"
           href="https://wspaper.org/s/tg">
            <img width="24" height="24" class="lazyload" alt="텔레그램 알림"
                 loading="lazy" src="<?= asset_path().'/icon-telegram-gray.svg' ?>">
        </a>
    </div>
    <?php
    $main_nav_items = include 'main-nav-items.php';
    $menus = [$main_nav_items];

    global $additional_mobile_menus;
    if (!empty($additional_mobile_menus)) {
        $menus = [$main_nav_items, ...$additional_mobile_menus];
    }

    global $additional_admin_menu;
    if (class_exists('Auth') and Auth::check() and !empty($additional_admin_menu)) {
        $menus = [$main_nav_items, ...$additional_mobile_menus, $additional_admin_menu];
    }

    foreach ($menus as $menu) { ?>
        <ul class="c-mobile-hamburger-menu__menu">
            <?php foreach ($menu as $item) { ?>
                <?php if ($item['label'] === '로그아웃') { ?>
                    <li class="c-mobile-hamburger-menu__item" <?= !empty($item['width']) ? "style='flex-basis: {$item['width']}'" : '' ?>>
                        <form action="{{ url('admin/logout') }}" method="post" class="c-mobile-hamburger-menu__link">
                            <button onclick="return confirm('로그아웃할까요?')" class="u-color-inherit  c-button-like-text">
                                <b>로그아웃</b>
                            </button>
                        </form>
                    </li>
                    <?php continue; ?>
                <?php } ?>
                <li class="c-mobile-hamburger-menu__item" <?= !empty($item['width']) ? "style='flex-basis: {$item['width']}'" : '' ?>>
                    <?php if (empty($item['sub_menus'])) { ?>
                        <a class="c-mobile-hamburger-menu__link"
                           href="<?= $item['url'] ?>">
                            <?= $item['hamburger_label'] ?>
                        </a>
                    <?php } else { ?>
                        <button
                            class="c-mobile-hamburger-menu__link  c-mobile-hamburger-menu__link--has-submenu  js-toggle-submenu">
                            <?= $item['hamburger_label'] ?>
                        </button>
                        <div class="c-mobile-hamburger-menu__submenu  js-submenu">
                            <?php foreach ($item['sub_menus'] as $sub_menu_items) { ?>
                                <div class="c-mobile-hamburger-menu__submenu-container">
                                    <?php foreach ($sub_menu_items as $sub_menu_item) { ?>
                                        <div class="c-mobile-hamburger-menu__submenu-item"
                                            <?= (count($sub_menu_items) <= 2) ? 'style="width: 50%;"' : '' ?>>
                                            <a href="<?= $sub_menu_item['url'] ?>"><?= $sub_menu_item['hamburger_label'] ?></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>

</div>
