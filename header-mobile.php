<?php
require_once __DIR__ . '/functions.php';
?>
<div class="c-header-mobile  u-no-print">
    <input type="checkbox" class="c-header-mobile__hamburger-checkbox" id="hamburger_checkbox">

    <label class="c-header-mobile__hamburger-button" for="hamburger_checkbox">
        <span class="c-header-mobile__hamburger-bar"></span>
        <span class="c-header-mobile__hamburger-bar"></span>
        <span class="c-header-mobile__hamburger-bar"></span>
    </label>

    <?php include 'mobile-hamburger-menu.php' ?>

    <div class='c-header-mobile__logo'>
        <a href='<?= org_url() ?>'>
            <img width="118" height="29" src='<?= asset_path('/logo-unified.svg') ?>' alt="노동자연대">
        </a>
    </div>

    <nav class="o-swipe-nav">
        <div class="o-swipe-nav__swiper">
            <?php include 'main-navigation-mobile.php' ?>
        </div>
        <div class="o-swipe-nav__clue  o-swipe-nav__clue--white"></div>
    </nav>

    <?php include 'main-navigation-mobile-submenus.php' ?>
</div>

<?php include 'search-form-mobile.php' ?>
