<?php
/**
 * @var array $main_nav
 */
?>

<?php
$main_nav_items = include 'main-nav-items.php';
foreach ($main_nav_items as $i => $item) { ?>
    <?php if (!empty($item['sub_menus'])) { ?>
        <div class="c-main-navigation__submenu-container  js-submenu-container" id="mobile-submenu-<?= $i ?>">
            <?php
            foreach ($item['sub_menus'] as $sub_menu_items) { ?>
                <div class="c-main-navigation__submenu  js-submenu">
                    <?php
                    foreach ($sub_menu_items as $sub_menu_item) { ?>
                        <div class="c-main-navigation__submenu-item">
                            <a class="c-main-navigation__submenu-link"
                               href="<?= $sub_menu_item['url'] ?>"><?= $sub_menu_item['mobile_label'] ?></a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
<?php } ?>
