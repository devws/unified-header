<?php
/**
 * @var array $main_nav
 */
?>

<ul class="c-main-navigation__container">
    <?php
    $main_nav_items = include 'main-nav-items.php';
    foreach ($main_nav_items as $i => $item) { ?>
    <li class="c-main-navigation__item  js-main-navigation-item  <?= empty($item['sub_menus']) ? '' : 'js-main-navigation-item-has-submenus' ?>">
        <?php if (empty($item['sub_menus'])) { ?>
            <a class="c-main-navigation__link  js-main-navigation-link  <?= $item['class_string'] ?>"
               href="<?= $item['url'] ?>">
                <?= $item['label'] ?>
            </a>
        <?php } else { ?>
            <a class="c-main-navigation__link  js-main-navigation-link  <?= $item['class_string'] ?>"
               href="<?= $item['url'] ?>">
                <?= $item['label'] ?>
            </a>
            <div class="c-main-navigation__submenu-container  js-submenu-container">
                <div class="c-main-navigation__submenu-line  js-submenu-line"></div>
                <?php
                foreach ($item['sub_menus'] as $sub_menu_items) { ?>
                    <div class="c-main-navigation__submenu">
                        <?php
                        foreach ($sub_menu_items as $sub_menu_item) { ?>
                            <div class="c-main-navigation__submenu-item">
                                <a class="c-main-navigation__submenu-link" href="<?= $sub_menu_item['url'] ?>"><?= $sub_menu_item['label'] ?></a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <?php } ?>
</ul>
