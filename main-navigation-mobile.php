<?php
/**
 * @var array $main_nav
 */
?>

<ul class="c-main-navigation__container">
    <?php
    $main_nav_items = include 'main-nav-items.php';
    foreach ($main_nav_items as $i => $item) { ?>
        <li class="c-main-navigation__item  js-main-navigation-item
            <?= empty($item['sub_menus']) ? '' : 'js-main-navigation-item-has-submenus' ?>">
            <?php if (empty($item['sub_menus'])) { ?>
                <a class="c-main-navigation__link  js-main-navigation-link  <?= $item['class_string'] ?>"
                   href="<?= $item['url'] ?>">
                    <?= $item['mobile_label'] ?>
                </a>
            <?php } else { ?>
                <button class="c-main-navigation__link  js-main-navigation-link  <?= $item['class_string'] ?>"
                        data-target-selector="#mobile-submenu-<?= $i ?>"><?= $item['mobile_label'] ?></button>
            <?php } ?>
        </li>
    <?php } ?>
</ul>
