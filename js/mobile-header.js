document.querySelectorAll('.js-mobile-hamburger-menu a').forEach(item => {
    item.addEventListener('click', () => {
        document.querySelector('#mobile-nav-toggle').checked = false;
    });
});

document.querySelectorAll('.js-mobile-hamburger-menu .js-toggle-submenu').forEach(item => {
    item.addEventListener('click', e => {
        item.parentNode.querySelector('.js-submenu').classList.toggle('c-mobile-hamburger-menu__submenu--visible');
        e.target.classList.toggle('c-mobile-hamburger-menu__link--opened-submenu');
    });
});
