function isTouchDevice() {
    return ('ontouchstart' in document.documentElement);
}

/**
 * 서브메뉴 열린 채로 메인 슬라이더가 움직이면 정신없어서, 서브메뉴를 열면 오토플레이를 멈추게 한다.
 */
function stopMainSliderAutoplay() {
    if (document.querySelector('.js-main-slider') && window.swiper && window.swiper.autoplay) {
        window.swiper.autoplay.stop();
    }
}

function openSubmenu(e) {
    e.preventDefault();
    menuItems.forEach(el => el.classList.remove('active'));
    const menuItem = e.target.closest('.js-main-navigation-item-has-submenus');
    if (!menuItem) {
        return;
    }
    const rect = menuItem.getBoundingClientRect();
    // 왼쪽 맞춰 줌.
    const submenuContainer = menuItem.querySelector('.js-submenu-container');
    const submenuLine = menuItem.querySelector('.js-submenu-line');
    const headerInner = document.querySelector('.js-header-inner');
    const headerInnerWidth = headerInner.clientWidth;
    const windowWidth = window.innerWidth;

    if (submenuContainer) {
        submenuContainer.style.left = rect.left * -1 + 'px';
        submenuContainer.style.paddingLeft = `calc(${rect.left}px - 10px)`;
        submenuLine.style.width = `calc(
                                          ${headerInnerWidth}px
                                          - ${rect.left}px
                                          + (${(windowWidth - headerInnerWidth) / 2}px
                                          - 30px
                                        )`;
    }
    menuItem.classList.add('active');

    stopMainSliderAutoplay();
}

function closeSubmenu(e) {
    e.preventDefault();
    const menuItem = e.target.closest('.js-main-navigation-item-has-submenus');
    if (!menuItem) {
        return;
    }
    menuItem.classList.remove('active');
}


const menuItemsLinks = document.querySelectorAll('.js-main-navigation-link');
const menuItems = document.querySelectorAll('.js-main-navigation-item');


if (!isTouchDevice()) {
    menuItems.forEach((mainNavigationItemLink) => {
        mainNavigationItemLink.addEventListener('mouseenter', openSubmenu);
        mainNavigationItemLink.closest('.js-main-navigation-item')
            .addEventListener('mouseleave', closeSubmenu);
    });

    menuItemsLinks.forEach((mainNavigationItemLink) => {
        mainNavigationItemLink.addEventListener('focus', openSubmenu);
    });
}

if (isTouchDevice()) {
    menuItemsLinks.forEach((mainNavigationItemLink) => {
        mainNavigationItemLink.addEventListener('click', e => {
            const submenuContainer = document.querySelector(e.target.dataset.targetSelector);
            if (!submenuContainer) {
                return;
            }
            e.preventDefault();
            if (submenuContainer.classList.contains('active')) {
                // 열린 상태면 닫는다.
                submenuContainer.classList.remove('active');
            } else {
                // 닫힌 상태면 연다.
                document.querySelectorAll('.js-submenu-container')
                    .forEach(el => el.classList.remove('active'))
                const leftPx = e.target.getBoundingClientRect().left;
                let submenuWidth = 0;
                submenuContainer.querySelectorAll('.js-submenu').forEach(el => submenuWidth += el.clientWidth)
                const paddingLeftMax = window.innerWidth - submenuWidth;

                // 왼쪽 맞춰 줌.
                submenuContainer.style.paddingLeft = `calc(${Math.min(leftPx, paddingLeftMax)}px - 9px)`;
                submenuContainer.classList.add('active');

                stopMainSliderAutoplay();
            }
        });
    });
}
