document.querySelector('.js-mobile-search-toggle').addEventListener('click', function (event) {
	if (document.querySelector('.js-mobile-search-toggle').checked === true) {
		document.querySelector('.js-mobile-search-cover').classList.add('c-mobile-search__cover--visible');
		document.querySelector('.js-mobile-search-input').focus();
	} else if (document.querySelector('.js-mobile-search-toggle').checked === false) {
		document.querySelector('.js-mobile-search-cover').classList.remove('c-mobile-search__cover--visible');
	}
})

document.querySelector('.js-mobile-search-cover').addEventListener('click', function (event) {
	if (document.querySelector('.js-mobile-search-toggle').checked === true) {
		document.querySelector('.js-mobile-search-cover').classList.remove('c-mobile-search__cover--visible');
		document.querySelector('.js-mobile-search-toggle').checked = false;
	}
})
