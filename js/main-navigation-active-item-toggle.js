let wspaperUrl = new URL(document.querySelector('a.js-wspaper').href);
let marx21Url = new URL(document.querySelector('a.js-marx21').href);
let currentUrl = new URL(location.href);

const mainNavigationLinks = document.querySelectorAll('.js-main-navigation-link');

if (currentUrl.hostname == wspaperUrl.hostname) {
    //신문
    document.querySelectorAll('.js-wspaper').forEach(function (el) {
        el.classList.add('active');
    });
}

if (currentUrl.hostname == marx21Url.hostname) {
    //마르크스21
    document.querySelectorAll('.js-marx21').forEach(function (el) {
        el.classList.add('active');
    });
}

if (document.location.href.includes('/p/forum')) {
    //온라인 토론회
    document.querySelectorAll('.js-forum').forEach(function (el) {
        el.classList.add('active');
    });
}

if (currentUrl.pathname == '/idea-and-theory') {
    //사상과 이론
    document.querySelectorAll('.js-idea-and-theory').forEach(function (el) {
        el.classList.add('active');
    });
}

if (document.querySelectorAll('.js-main-navigation-link.active').length === 0 && currentUrl.pathname != '/') {
    //단체
    //active 된 네비게이션 메뉴가 없고 홈(/)이 아니면 메인네비게이션의 단체 소개 메뉴를 활성화한다.
    document.querySelectorAll('.js-ws').forEach(function (el) {
        el.classList.add('active');
    });
}
