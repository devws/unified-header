<?php
return [
    [
        'class_string'    => 'js-ws',
        'label'           => '단체 소개',
        'hamburger_label' => '단체 소개',
        'mobile_label'    => '단체',
        'url'             => org_url()."/about",
        'sub_menus'       => [
            [
                [
                    'label'           => '소개',
                    'hamburger_label' => '소개',
                    'mobile_label'    => '소개',
                    'url'             => org_url()."/about",
                ],

                [
                    'label'           => '가입하기',
                    'hamburger_label' => '가입하기',
                    'mobile_label'    => '가입하기',
                    'url'             => org_url()."/wp-content/themes/ws/join.php",
                ],

                [
                    'label'           => '후원하기',
                    'hamburger_label' => '후원하기',
                    'mobile_label'    => '후원하기',
                    'url'             => org_url()."/wp-content/themes/ws/support.php",
                ],

                [
                    'label'           => '연락·문의',
                    'hamburger_label' => '연락·문의',
                    'mobile_label'    => '연락·문의',
                    'url'             => org_url()."/contact",
                ],
            ],
            [
                [
                    'label'           => '소책자',
                    'hamburger_label' => '소책자',
                    'mobile_label'    => '소책자',
                    'url'             => org_url()."/p/book",
                ],

                [
                    'label'           => '사진',
                    'hamburger_label' => '사진',
                    'mobile_label'    => '사진',
                    'url'             => org_url()."/p/category/photo",
                ],

                //                [
                //                    'label' => '자료실',
                //                    'hamburger_label' => '자료실',
                //                    'mobile_label' => '자료실',
                //                    'url' => org_url() . "/p/category/material",
                //                ],

                [
                    'label'           => '‘맑시즘’ 포럼',
                    'hamburger_label' => '‘맑시즘’ 포럼',
                    'mobile_label'    => '‘맑시즘’ 포럼',
                    'url'             => 'https://marxism.or.kr',
                ],
            ],
            [
                [
                    'label'           => '규칙',
                    'hamburger_label' => '규칙',
                    'mobile_label'    => '규칙',
                    'url'             => org_url()."/rule",
                ],

                [
                    'label'           => '회원윤리강령',
                    'hamburger_label' => '회원윤리강령',
                    'mobile_label'    => '회원윤리강령',
                    'url'             => org_url()."/code-of-ethics",
                ],

                [
                    'label'           => '분쟁위 절차 규정 및<br>관련 용어 해설',
                    'hamburger_label' => '분쟁위 절차 규정 및 관련 용어 해설',
                    'mobile_label'    => '분쟁위 절차 규정 및<br>관련 용어 해설',
                    'url'             => org_url()."/규율과분쟁조정위원회-절차-규정-및-관련-용어-해설",
                ],
            ],
        ]
    ],
    [
        'class_string'    => 'js-wspaper',
        'label'           => '〈노동자 연대〉 신문',
        'hamburger_label' => '〈노동자 연대〉 신문',
        'mobile_label'    => '신문',
        'url'             => newspaper_url(),
        'sub_menus'       => [
            [
                [
                    'label'           => '신문 홈',
                    'hamburger_label' => '신문 홈',
                    'mobile_label'    => '신문 홈',
                    'url'             => newspaper_url(),
                ],
                [
                    'label'           => '신문 소개',
                    'hamburger_label' => '신문 소개',
                    'mobile_label'    => '신문 소개',
                    'url'             => newspaper_url().'/page/about',
                ],
                [
                    'label'           => '종이 신문 정기구독',
                    'hamburger_label' => '종이 신문 정기구독',
                    'mobile_label'    => '종이 신문 정기구독',
                    'url'             => newspaper_url().'/subs',
                ],
                [
                    'label'           => '온라인 채널 안내',
                    'hamburger_label' => '온라인 채널 안내',
                    'mobile_label'    => '온라인 채널 안내',
                    'url'             => newspaper_url().'/online',
                ],
                [
                    'label'           => '구입처 안내',
                    'hamburger_label' => '구입처 안내',
                    'mobile_label'    => '구입처 안내',
                    'url'             => newspaper_url().'/page/where-to-buy',
                ],
                [
                    'label'           => '후원하기',
                    'hamburger_label' => '후원하기',
                    'mobile_label'    => '후원하기',
                    'url'             => newspaper_url().'/donation',
                ],
            ]

        ]
    ],
    [
        'class_string'    => 'js-wstv',
        'label'           => '노동자연대TV',
        'hamburger_label' => '노동자연대TV',
        'mobile_label'    => '노동자연대TV',
        'url'             => 'https://youtube.com/c/노동자연대TV',
        'sub_menus'       => [
            [
                [
                    'label'           => '채널 홈',
                    'hamburger_label' => '채널 홈',
                    'mobile_label'    => '채널 홈',
                    'url'             => 'https://youtube.com/c/노동자연대TV',
                ],
                [
                    'label'           => '채널 소개',
                    'hamburger_label' => '채널 소개',
                    'mobile_label'    => '채널 소개',
                    'url'             => 'https://www.youtube.com/watch?v=zWMX3Op_K_w',
                ],
                [
                    'label'           => '온라인 토론회 영상',
                    'hamburger_label' => '온라인 토론회 영상',
                    'mobile_label'    => '온라인 토론회 영상',
                    'url'             => 'https://www.youtube.com/playlist?list=PLXWnWki5neClaeZZnoOeDzJdFZ86W-j6N',
                ],
                [
                    'label'           => '시사/이슈 톡톡',
                    'hamburger_label' => '시사/이슈 톡톡',
                    'mobile_label'    => '시사/이슈 톡톡',
                    'url'             => 'https://www.youtube.com/playlist?list=PLXWnWki5neCmg39abvoSMLrIUJQKeMOjt',
                ],
                [
                    'label'           => '기후 위기! 체제를 바꾸자 시리즈(10회)',
                    'hamburger_label' => '기후 위기! 체제를 바꾸자 시리즈(10회)',
                    'mobile_label'    => '기후 위기! 체제를 바꾸자 시리즈(10회)',
                    'url'             => 'https://www.youtube.com/playlist?list=PLXWnWki5neClcFs8NvWNBJR6XEQgZyqvy',
                ],
                [
                    'label'           => '맑시즘 강연 음원',
                    'hamburger_label' => '맑시즘 강연 음원',
                    'mobile_label'    => '맑시즘 강연 음원',
                    'url'             => 'https://www.youtube.com/playlist?list=PLXWnWki5neCkEIvlSpfbdSRNoUm8mOTAz',
                ],
            ]
        ]
    ],
    [
        'class_string'    => 'js-forum',
        'label'           => '온라인 토론회',
        'hamburger_label' => '온라인 토론회',
        'mobile_label'    => '온라인 토론회',
        'url'             => org_url().'/p/forum',
    ],
    [
        'class_string'    => 'js-marx21',
        'label'           => '<small class="c-main-navigation__ruby">계간</small>《마르크스21》',
        'hamburger_label' => '계간 《마르크스21》',
        'mobile_label'    => '계간지',
        'url'             => journal_url(),
        'sub_menus'       => [
            [
                [
                    'label'           => '《마르크스21》 홈',
                    'hamburger_label' => '홈',
                    'mobile_label'    => '《마르크스21》 홈',
                    'url'             => journal_url(),
                ],
                [
                    'label'           => '《마르크스21》 소개',
                    'hamburger_label' => '소개',
                    'mobile_label'    => '《마르크스21》 소개',
                    'url'             => journal_url().'/about',
                ],
                [
                    'label'           => '정기구독',
                    'hamburger_label' => '정기구독',
                    'mobile_label'    => '정기구독',
                    'url'             => journal_url().'/subscription/form',
                ],
                [
                    'label'           => '낱권 구입',
                    'hamburger_label' => '낱권 구입',
                    'mobile_label'    => '낱권 구입',
                    'url'             => journal_url().'/buy/form',
                ],
                [
                    'label'           => '후원하기',
                    'hamburger_label' => '후원하기',
                    'mobile_label'    => '후원하기',
                    'url'             => journal_url().'/donation/once-form',
                ],
            ]

        ]
    ],
    [
        'class_string'    => 'js-idea-and-theory',
        'label'           => '사상과 이론',
        'hamburger_label' => '사상과 이론',
        'mobile_label'    => '사상과 이론',
        'url'             => org_url().'/idea-and-theory',
    ],
    [
        'class_string'    => 'js-youth',
        'label'           => '청년학생그룹',
        'hamburger_label' => '청년학생그룹',
        'mobile_label'    => '청년학생그룹',
        'url'             => 'https://stu.workerssolidarity.org',
        'sub_menus'       => [
            [
                [
                    'label'           => '청년학생그룹 홈',
                    'hamburger_label' => '홈',
                    'mobile_label'    => '청년학생그룹 홈',
                    'url'             => 'https://stu.workerssolidarity.org',
                ],
                [
                    'label'           => '소개',
                    'hamburger_label' => '소개',
                    'mobile_label'    => '소개',
                    'url'             => 'https://stu.workerssolidarity.org/%eb%85%b8%eb%8f%99%ec%9e%90%ec%97%b0%eb%8c%80-%ed%95%99%ec%83%9d%ea%b7%b8%eb%a3%b9%ec%9d%84-%ec%86%8c%ea%b0%9c%ed%95%a9%eb%8b%88%eb%8b%a4',
                ],
                [
                    'label'           => '성명',
                    'hamburger_label' => '성명',
                    'mobile_label'    => '성명',
                    'url'             => 'https://stu.workerssolidarity.org/?cat=603&only-stugroup=1',
                ],
                [
                    'label'           => '읽을 거리',
                    'hamburger_label' => '읽을 거리',
                    'mobile_label'    => '읽을 거리',
                    'url'             => 'https://stu.workerssolidarity.org/a/category/%ec%9d%bd%ec%9d%84%ea%b1%b0%eb%a6%ac',
                ],
                [
                    'label'           => '활동 보고',
                    'hamburger_label' => '활동 보고',
                    'mobile_label'    => '활동 보고',
                    'url'             => 'https://stu.workerssolidarity.org/a/category/%ed%99%9c%eb%8f%99-%eb%b3%b4%ea%b3%a0',
                ],
                [
                    'label'           => '토론회',
                    'hamburger_label' => '토론회',
                    'mobile_label'    => '토론회',
                    'url'             => 'https://stu.workerssolidarity.org/a/category/%ed%86%a0%eb%a1%a0%ed%9a%8c',
                ],
            ]

        ]
    ],
];
