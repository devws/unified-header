# 통합 헤더 패키지

## 신문, 저널

패키지의 `images` 폴더를 `public`에 링크해야 한다. `js`와 `sass` 밑의 파일들은 컴파일하면 되니 링크할 필요는 없다.

```bash
ln -s ../vendor/devws/unified-header/images public/unified-hedaer-images
```

## 단체

딱히 해 줘야 할 것은 없다. `vendor/devws/unified-header/` 밑에 있는 폴더와 파일들을 사용하면 된다.
