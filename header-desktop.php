<?php
require_once __DIR__ . '/functions.php';
?>
<header class='c-header  u-no-print'>
    <div class="c-header__inner  js-header-inner">
        <div class='c-header__logo'>
            <a href='<?= org_url() ?>'>
                <img src='<?= asset_path('logo-unified.svg') ?>' alt="노동자연대">
            </a>
        </div>

        <?php
        $main_nav = include( 'main-nav-items.php' );
        ?>
        <div class="c-main-navigation" id='main-nav'>
            <?php include 'main-navigation-desktop.php' ?>
        </div>

        <?php
        if (uh_need_header_search_form()) {
            // 조건에 따라 헤더의 검색창 노출 여부 결정.
            ?>
            <form role="search" method="get" class="c-search-form  c-search-form--hidden-on-narrow"
                  action="<?= uh_search_form_action() ?>">
                <input class="c-search-form__input  c-search-form__input--gray" type="search" id="search"
                       name="<?= uh_search_input_name() ?>" value="<?= $_GET[uh_search_input_name()] ?? '' ?>"
                       placeholder="검색어를 입력하세요">
                <button class="c-search-form__submit" type="submit">
                    <img width="16" height="16" src="<?= asset_path('icon-magnifying-glass.svg') ?>" alt="">
                </button>
            </form>
        <?php } ?>
    </div>
</header>
